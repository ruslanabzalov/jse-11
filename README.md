# JSE TASK MANAGER

## About Application
Task Manager Application

## About Developer
* **Full Name**: Ruslan Abzalov
* **E-Mail**: ruslanonetwo@gmail.com
* **Company**: Technoserv Consulting

## Hardware
* **CPU**: Intel Core i7-7500U @ 2.70GHz x 4
* **RAM**: 8GB
* **Disk Capacity**: SSD 256GB

## Software
* **OS Name**: Ubuntu 20.04.1 LTS
* **OS Type**: 64-bit
* **JDK**: 1.8+

## How To Build
```
$ mvn clean compile assembly:single
```
## How To Run
```
$ java -jar target/task-manager-1.0-jar-with-dependencies.jar
```

## Tasks result
* **JSE-00**: [jse-00](https://drive.google.com/drive/folders/1vwK52B_H550TXXs2pLqFJfb83yswBuj4?usp=sharing)
* **JSE-01**: [jse-01-task-manager-result.png](https://drive.google.com/file/d/1w3qYGjnhBTM0jpmxhF_G_OPoruwfJD99/view?usp=sharing)
* **JSE-02**: [jse-02-task-manager-result.png](https://drive.google.com/file/d/1h1hVvMiSoqK0iVvXO9F1ESBWBAJN5K8H/view?usp=sharing)
* **JSE-03**: [jse-03](https://drive.google.com/drive/folders/14YWFA2tSnaPmDJH-tx6XaPdG_ElknKYD?usp=sharing)
* **JSE-04**: [jse-04](https://drive.google.com/drive/folders/1PSmSwD7qiUNZlO_5cA5DgIXau0NPX1SG?usp=sharing)
* **JSE-05**: [jse-05](https://drive.google.com/drive/folders/1jLZviggFBpyeQBGrD8mLh0zXNoVc3ob7?usp=sharing)
* **JSE-06**: [jse-06](https://drive.google.com/drive/folders/1LzHcWtbE9folIBtpN89YllMR1dbarbVc?usp=sharing)
* **JSE-07**: [jse-07](https://drive.google.com/drive/folders/1wkPvDiUENQDdC45F4vbRw2NagDSP6NsQ?usp=sharing)
* **JSE-08**: [jse-08](https://drive.google.com/drive/folders/1WKYB-6hmNcAgUzt2r5b_TG65l-ZXxYuG?usp=sharing)
* **JSE-09**: [jse-09](https://drive.google.com/drive/folders/1NTER8GgpxVTCnHijiqeMMcgpikVFz-qm?usp=sharing)
* **JSE-10**: [jse-10](https://drive.google.com/drive/folders/1MP8Yfb2w3ArMKq799QXFyWqNqtp1_WsP?usp=sharing)
* **JSE-11**: [jse-11](https://drive.google.com/drive/folders/1BlkEHdl9wIrUPxI61EG_Jfj0MfI_F9Ee?usp=sharing)