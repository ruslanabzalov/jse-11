package tsc.abzalov.tm.controller;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.controller.ICommandController;
import tsc.abzalov.tm.api.service.ICommandService;
import tsc.abzalov.tm.model.Command;
import tsc.abzalov.tm.model.enumeration.CommandType;

import static java.util.Arrays.stream;
import static tsc.abzalov.tm.model.enumeration.CommandType.*;
import static tsc.abzalov.tm.util.Formatter.formatBytes;

public class CommandController implements ICommandController {

    @NotNull
    private final ICommandService commandService;

    public CommandController(@NotNull ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showHelp() {
        final Command[] commands = commandService.getCommands();
        String resultHelpText = groupCommandsByTypes(commands, BASIC_COMMAND) +
                groupCommandsByTypes(commands, PROJECT_COMMAND) +
                groupCommandsByTypes(commands, TASK_COMMAND);

        System.out.print(resultHelpText);
    }

    @Override
    public void showInfo() {
        final int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("Cores: " + cores);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        System.out.println("Max Memory: " + formatBytes(maxMemory));

        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total Memory: " + formatBytes(totalMemory));

        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free Memory: " + formatBytes(freeMemory));

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used Memory: " + formatBytes(usedMemory) + "\n");
    }

    @Override
    public void showAbout() {
        System.out.println("Developer Full Name: Ruslan Abzalov");
        System.out.println("Developer Email: rabzalov@tsconsulting.com\n");
    }

    @Override
    public void showVersion() {
        System.out.println("Version: 1.0.0\n");
    }

    @Override
    public void showCommandNames() {
        stream(commandService.getCommandNames()).forEach(System.out::println);
        System.out.println();
    }

    @Override
    public void showCommandArgs() {
        stream(commandService.getCommandArgs()).forEach(System.out::println);
        System.out.println();
    }

    @Override
    public void showError(@NotNull String input, boolean isArg) {
        if (isArg)
            System.out.println("Argument \"" + input + "\" is not available!\n" + "Please, use \"-h\" argument.\n");
        else
            System.out.println("Command \"" + input + "\" is not available!\n" + "Please, use \"help\" command.\n");
    }

    @Override
    public void exit() {
        System.out.println("Application Is Closing...");
        System.exit(0);
    }

    @NotNull
    private String groupCommandsByTypes(@NotNull Command[] commands, @NotNull CommandType commandType) {
        final StringBuilder builder = new StringBuilder();
        switch (commandType) {
            case BASIC_COMMAND:
                builder.append("[BASIC COMMANDS]\n");
                break;
            case PROJECT_COMMAND:
                builder.append("[PROJECT COMMANDS]\n");
                break;
            case TASK_COMMAND:
                builder.append("[TASK COMMANDS]\n");
                break;
            default:
                return "";
        }

        for (final Command command : commands) {
            if (command.getCommandType().equals(commandType))
                builder.append(command.toString()).append("\n");
        }

        return builder.append("\n").toString();
    }

}
