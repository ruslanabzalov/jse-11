package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.model.Command;
import tsc.abzalov.tm.model.enumeration.TaskManagerUnit;

import static java.util.Arrays.stream;
import static tsc.abzalov.tm.constant.ApplicationArgumentConst.*;
import static tsc.abzalov.tm.constant.ApplicationCommandConst.*;
import static tsc.abzalov.tm.model.enumeration.CommandType.*;
import static tsc.abzalov.tm.model.enumeration.TaskManagerUnit.ARGUMENT;
import static tsc.abzalov.tm.model.enumeration.TaskManagerUnit.COMMAND;

public class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            CMD_HELP, ARG_HELP, "Shows all available commands.", BASIC_COMMAND
    );

    private static final Command INFO = new Command(
            CMD_INFO, ARG_INFO, "Shows CPU and RAM info.", BASIC_COMMAND
    );

    private static final Command ABOUT = new Command(
            CMD_ABOUT, ARG_ABOUT, "Shows developer info.", BASIC_COMMAND
    );

    private static final Command VERSION = new Command(
            CMD_VERSION, ARG_VERSION, "Shows application version.", BASIC_COMMAND
    );

    private static final Command COMMANDS = new Command(
            CMD_COMMANDS, ARG_COMMANDS, "Show all available commands.", BASIC_COMMAND
    );

    private static final Command ARGUMENTS = new Command(
            CMD_ARGUMENTS, ARG_ARGUMENTS, "Show all available arguments.", BASIC_COMMAND
    );

    private static final Command EXIT = new Command(
            CMD_EXIT, null, "Shutdowns application.", BASIC_COMMAND
    );

    private static final Command CREATE_PROJECT = new Command(
            CMD_CREATE_PROJECT, null, "Create project.", PROJECT_COMMAND
    );

    private static final Command SHOW_ALL_PROJECTS = new Command(
            CMD_SHOW_ALL_PROJECTS, null, "Show all projects.", PROJECT_COMMAND
    );

    private static final Command SHOW_PROJECT_BY_ID = new Command(
            CMD_SHOW_PROJECT_BY_ID, null, "Show project by id.", PROJECT_COMMAND
    );

    private static final Command SHOW_PROJECT_BY_INDEX = new Command(
            CMD_SHOW_PROJECT_BY_INDEX, null, "Show project by index.", PROJECT_COMMAND
    );

    private static final Command SHOW_PROJECTS_BY_NAME = new Command(
            CMD_SHOW_PROJECTS_BY_NAME, null, "Show projects by name.", PROJECT_COMMAND
    );

    private static final Command UPDATE_PROJECT_BY_ID = new Command(
            CMD_UPDATE_PROJECT_BY_ID, null, "Update project by id.", PROJECT_COMMAND
    );

    private static final Command UPDATE_PROJECT_BY_INDEX = new Command(
            CMD_UPDATE_PROJECT_BY_INDEX, null, "Update project by index.", PROJECT_COMMAND
    );

    private static final Command DELETE_ALL_PROJECTS = new Command(
            CMD_DELETE_ALL_PROJECTS, null, "Delete all projects.", PROJECT_COMMAND
    );

    private static final Command DELETE_PROJECT_BY_ID = new Command(
            CMD_DELETE_PROJECT_BY_ID, null, "Delete project by id.", PROJECT_COMMAND
    );

    private static final Command DELETE_PROJECT_BY_INDEX = new Command(
            CMD_DELETE_PROJECT_BY_INDEX, null, "Delete project by index.", PROJECT_COMMAND
    );

    private static final Command DELETE_PROJECTS_BY_NAME = new Command(
            CMD_DELETE_PROJECTS_BY_NAME, null, "Delete projects by name.", PROJECT_COMMAND
    );

    private static final Command CREATE_TASK = new Command(
            CMD_CREATE_TASK, null, "Create task.", TASK_COMMAND
    );

    private static final Command SHOW_ALL_TASKS = new Command(
            CMD_SHOW_ALL_TASKS, null, "Show all tasks.", TASK_COMMAND
    );

    private static final Command SHOW_TASK_BY_ID = new Command(
            CMD_SHOW_TASK_BY_ID, null, "Show task by id.", TASK_COMMAND
    );

    private static final Command SHOW_TASK_BY_INDEX = new Command(
            CMD_SHOW_TASK_BY_INDEX, null, "Show task by index.", TASK_COMMAND
    );

    private static final Command SHOW_TASKS_BY_NAME = new Command(
            CMD_SHOW_TASKS_BY_NAME, null, "Show tasks by name.", TASK_COMMAND
    );

    private static final Command UPDATE_TASK_BY_ID = new Command(
            CMD_UPDATE_TASK_BY_ID, null, "Update task by id.", TASK_COMMAND
    );

    private static final Command UPDATE_TASK_BY_INDEX = new Command(
            CMD_UPDATE_TASK_BY_INDEX, null, "Update TASK by index.", TASK_COMMAND
    );

    private static final Command DELETE_ALL_TASKS = new Command(
            CMD_DELETE_ALL_TASKS, null, "Delete all tasks.", TASK_COMMAND
    );

    private static final Command DELETE_TASK_BY_ID = new Command(
            CMD_DELETE_TASK_BY_ID, null, "Delete task by id.", TASK_COMMAND
    );

    private static final Command DELETE_TASK_BY_INDEX = new Command(
            CMD_DELETE_TASK_BY_INDEX, null, "Delete task by index.", TASK_COMMAND
    );

    private static final Command DELETE_TASKS_BY_NAME = new Command(
            CMD_DELETE_TASKS_BY_NAME, null, "Delete tasks by name.", TASK_COMMAND
    );

    private static final Command[] ALL_COMMANDS = {
            HELP, INFO, ABOUT, VERSION, COMMANDS, ARGUMENTS, EXIT,
            CREATE_PROJECT, SHOW_ALL_PROJECTS, SHOW_PROJECT_BY_ID, SHOW_PROJECT_BY_INDEX, SHOW_PROJECTS_BY_NAME,
            UPDATE_PROJECT_BY_ID, UPDATE_PROJECT_BY_INDEX, DELETE_ALL_PROJECTS, DELETE_PROJECT_BY_ID,
            DELETE_PROJECT_BY_INDEX, DELETE_PROJECTS_BY_NAME,
            CREATE_TASK, SHOW_ALL_TASKS, SHOW_TASK_BY_ID, SHOW_TASK_BY_INDEX, SHOW_TASKS_BY_NAME,
            UPDATE_TASK_BY_ID, UPDATE_TASK_BY_INDEX, DELETE_ALL_TASKS, DELETE_TASK_BY_ID,
            DELETE_TASK_BY_INDEX, DELETE_TASKS_BY_NAME
    };

    @Override
    @NotNull
    public Command[] getCommands() {
        return ALL_COMMANDS;
    }

    @Override
    @NotNull
    public String[] getCommandNames() {
        return getParamsByUnit(COMMAND);
    }

    @Override
    @NotNull
    public String[] getCommandArgs() {
        return getParamsByUnit(ARGUMENT);
    }

    @NotNull
    private String[] getParamsByUnit(@NotNull TaskManagerUnit unit) {
        final Command[] filteredCommands = (unit.equals(COMMAND))
                ? stream(ALL_COMMANDS)
                    .toArray(Command[]::new)
                : stream(ALL_COMMANDS)
                    .filter(command -> !(command.getArg() == null || command.getArg().isEmpty()))
                    .toArray(Command[]::new);
        final String[] resultParams = new String[filteredCommands.length];
        String currentParam;

        for (int i = 0; i < filteredCommands.length; i++) {
            currentParam = (unit.equals(COMMAND)) ? filteredCommands[i].getName() : filteredCommands[i].getArg();
            resultParams[i] = currentParam;
        }

        return resultParams;
    }

}
