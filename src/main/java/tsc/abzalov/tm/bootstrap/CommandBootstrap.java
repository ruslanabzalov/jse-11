package tsc.abzalov.tm.bootstrap;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import tsc.abzalov.tm.api.controller.ICommandController;
import tsc.abzalov.tm.api.controller.ISubjectController;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.repository.ISubjectRepository;
import tsc.abzalov.tm.api.service.ICommandService;
import tsc.abzalov.tm.api.service.ISubjectService;
import tsc.abzalov.tm.controller.CommandController;
import tsc.abzalov.tm.controller.ProjectController;
import tsc.abzalov.tm.controller.TaskController;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.repository.CommandRepository;
import tsc.abzalov.tm.repository.SubjectRepository;
import tsc.abzalov.tm.service.CommandService;
import tsc.abzalov.tm.service.SubjectService;

import static tsc.abzalov.tm.constant.ApplicationArgumentConst.*;
import static tsc.abzalov.tm.constant.ApplicationCommandConst.*;
import static tsc.abzalov.tm.util.InputUtil.INPUT;

public class CommandBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ISubjectRepository<Project> projectRepository = new SubjectRepository<>();

    private final ISubjectRepository<Task> taskRepository = new SubjectRepository<>();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ISubjectService<Project> projectService = new SubjectService<>(projectRepository);

    private final ISubjectService<Task> taskService = new SubjectService<>(taskRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ISubjectController projectController = new ProjectController(projectService);

    private final ISubjectController taskController = new TaskController(taskService);

    public void run(String... args) {
        System.out.println();
        if (areArgExists(args)) return;

        System.out.println("****** WELCOME TO TASK MANAGER APPLICATION ******");
        System.out.println("Please, use \"help\" command to see all available commands.\n");

        String command;
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.print("Please, enter your command: ");
            command = INPUT.nextLine();
            System.out.println();
            if (StringUtils.isEmpty(command)) continue;
            processCommand(command);
        }
    }

    private boolean areArgExists(String... args) {
        if (ArrayUtils.isEmpty(args)) return false;

        final String arg = args[0];
        if (StringUtils.isEmpty(arg)) return false;

        processArg(arg);
        return true;
    }

    private void processArg(String arg) {
        switch (arg) {
            case ARG_HELP:
                commandController.showHelp();
                break;
            case ARG_INFO:
                commandController.showInfo();
                break;
            case ARG_ABOUT:
                commandController.showAbout();
                break;
            case ARG_VERSION:
                commandController.showVersion();
                break;
            case ARG_COMMANDS:
                commandController.showCommandNames();
                break;
            case ARG_ARGUMENTS:
                commandController.showCommandArgs();
                break;
            default:
                commandController.showError(arg, true);
        }
    }

    private void processCommand(String command) {
        switch (command) {
            case CMD_HELP:
                commandController.showHelp();
                break;
            case CMD_INFO:
                commandController.showInfo();
                break;
            case CMD_ABOUT:
                commandController.showAbout();
                break;
            case CMD_VERSION:
                commandController.showVersion();
                break;
            case CMD_COMMANDS:
                commandController.showCommandNames();
                break;
            case CMD_ARGUMENTS:
                commandController.showCommandArgs();
                break;
            case CMD_CREATE_PROJECT:
                projectController.create();
                break;
            case CMD_SHOW_ALL_PROJECTS:
                projectController.showAll();
                break;
            case CMD_SHOW_PROJECT_BY_ID:
                projectController.showById();
                break;
            case CMD_SHOW_PROJECT_BY_INDEX:
                projectController.showByIndex();
                break;
            case CMD_SHOW_PROJECTS_BY_NAME:
                projectController.showByName();
                break;
            case CMD_UPDATE_PROJECT_BY_ID:
                projectController.editById();
                break;
            case CMD_UPDATE_PROJECT_BY_INDEX:
                projectController.editByIndex();
                break;
            case CMD_DELETE_ALL_PROJECTS:
                projectController.removeAll();
                break;
            case CMD_DELETE_PROJECT_BY_ID:
                projectController.removeById();
                break;
            case CMD_DELETE_PROJECT_BY_INDEX:
                projectController.removeByIndex();
                break;
            case CMD_DELETE_PROJECTS_BY_NAME:
                projectController.removeByName();
                break;
            case CMD_CREATE_TASK:
                taskController.create();
                break;
            case CMD_SHOW_ALL_TASKS:
                taskController.showAll();
                break;
            case CMD_SHOW_TASK_BY_ID:
                taskController.showById();
                break;
            case CMD_SHOW_TASK_BY_INDEX:
                taskController.showByIndex();
                break;
            case CMD_SHOW_TASKS_BY_NAME:
                taskController.showByName();
                break;
            case CMD_UPDATE_TASK_BY_ID:
                taskController.editById();
                break;
            case CMD_UPDATE_TASK_BY_INDEX:
                taskController.editByIndex();
                break;
            case CMD_DELETE_ALL_TASKS:
                taskController.removeAll();
                break;
            case CMD_DELETE_TASK_BY_ID:
                taskController.removeById();
                break;
            case CMD_DELETE_TASK_BY_INDEX:
                taskController.removeByIndex();
                break;
            case CMD_DELETE_TASKS_BY_NAME:
                taskController.removeByName();
                break;
            case CMD_EXIT:
                commandController.exit();
            default:
                commandController.showError(command, false);
        }
    }

}
