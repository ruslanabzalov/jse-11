package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;

public class Project extends Subject {

    private Project() {
    }

    public Project(@NotNull String name) {
        this.name = name;
    }

    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

}
