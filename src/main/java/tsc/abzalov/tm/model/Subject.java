package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public abstract class Subject {

    @NotNull
    protected String id = UUID.randomUUID().toString();

    @NotNull
    protected String name = "";

    @NotNull
    protected String description = "Description is empty.";

    @NotNull
    public String getId() {
        return this.id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return this.name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.name + ": [ID: " + this.id + ", Description: " + this.description + "]";
    }

}
