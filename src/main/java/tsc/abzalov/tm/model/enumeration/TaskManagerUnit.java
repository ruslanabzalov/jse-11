package tsc.abzalov.tm.model.enumeration;

import org.jetbrains.annotations.NotNull;

public enum TaskManagerUnit {

    COMMAND("Command"),
    ARGUMENT("Argument");

    @NotNull
    private final String name;

    TaskManagerUnit(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getName() {
        return this.name;
    }

}
