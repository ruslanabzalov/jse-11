package tsc.abzalov.tm.api.repository;

import tsc.abzalov.tm.model.Subject;

import java.util.List;

public interface ISubjectRepository<T extends Subject> {

    int size();

    boolean add(T t);

    List<T> findAll();

    T findById(String id);

    T findByIndex(int index);

    List<T> findByName(String name);

    boolean updateById(String id, String name, String description);

    boolean updateByIndex(int index, String name, String description);

    void deleteAll();

    boolean deleteById(String id);

    boolean deleteByIndex(int index);

    boolean deleteByName(String name);

}
