package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

    String[] getCommandNames();

    String[] getCommandArgs();

}
