package tsc.abzalov.tm.api.controller;

public interface ICommandController {

    void showHelp();

    void showInfo();

    void showAbout();

    void showVersion();

    void showCommandNames();

    void showCommandArgs();

    void showError(String input, boolean isArg);

    void exit();

}
