package tsc.abzalov.tm.util;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public class InputUtil {

    public static final Scanner INPUT = new Scanner(System.in);

    public static boolean isPositiveIntDigit(@NotNull String someString) {
        final boolean isDigit = someString.matches("^\\d+$");
        return isDigit && Integer.parseInt(someString) > 0;
    }

    public static boolean isCorrectIndex(int index, int listSize) {
        return index >= 0 && index <= listSize - 1;
    }

    public static String inputId() {
        System.out.print("Please, enter an ID: ");
        String projectId = INPUT.nextLine();
        while (StringUtils.isBlank(projectId)) {
            System.out.print("ID cannot be empty! Please, enter an correct ID: ");
            projectId = INPUT.nextLine();
        }
        return projectId;
    }

    public static int inputIndex() {
        System.out.print("Please, enter an index: ");
        String projectIndex = INPUT.nextLine();
        boolean isPositiveIntDigit = isPositiveIntDigit(projectIndex);
        while (!isPositiveIntDigit) {
            System.out.print("Incorrect index! Please, enter correct one: ");
            projectIndex = INPUT.nextLine();
            isPositiveIntDigit = isPositiveIntDigit(projectIndex);
        }
        return Integer.parseInt(projectIndex) - 1;
    }

    public static String inputName() {
        System.out.print("Please, enter a name: ");
        String projectName = INPUT.nextLine();
        while (StringUtils.isBlank(projectName)) {
            System.out.print("Name cannot be empty! Please, enter correct name: ");
            projectName = INPUT.nextLine();
        }
        return projectName;
    }

    public static String inputDescription() {
        System.out.print("Please, enter a description: ");
        return INPUT.nextLine();
    }

}
